<?php

/**
 * @file
 * The commands to interact with the Yuboto HTTP API.
 */

/**
 * Send callback for SMS Framework. Does the actual SMS sending.
 *
 * @param int $number     The number to send an SMS to, including country code, without 00 or +.
 * @param string $message Plain text SMS message.
 * @param array $options  SMS Framework send form values.
 *
 * @return mixed          Error array on failure, boolean TRUE on success.
 */
function _sms_yuboto_command_send($number, $message, array $options) {
  $settings = _sms_yuboto_get_configuration();

  list($date_later, $time_later) = _sms_yuboto_transform_date($options['yuboto_send_later']);

  // Note that array_filter will remove values that evaluate to FALSE.
  // Yuboto will use defaults for those values.
  $params = array_filter(array(
    'api_key'        => $settings['yuboto_api_key'],
    'action'         => 'Send',
    'from'           => $options['yuboto_send_name'],
    'to'             => $number,
    'text'           => trim($message),
    'typesms'        => $options['yuboto_send_type'] ? $options['yuboto_send_type'] : FALSE,
    'longsms'        => $settings['yuboto_long_sms'] ? 'yes' : FALSE,
    'datein_to_send' => $date_later,
    'timein_to_send' => $time_later,
    'smsValidity'    => $settings['yuboto_validity'],
    'smstest'        => $settings['yuboto_devel'] ? 'yes' : FALSE,
    'callback_url'   => url('sms/yuboto/callback', array('absolute' => TRUE)),
    'option1'        => variable_get('yuboto_install_id', ''),
    'option2'        => '',
    'json'           => 'Yes',
  ));

  $req = _sms_yuboto_request($params, $settings['yuboto_devel']);

  // Check if the HTTP call went through.
  if ($req->status_message != 'OK') {
    return array(
      'status' => FALSE,
      'message' => 'Could not connect to service',
    );
  }

  // Check if the Yuboto service returned success:
  if (isset($req->data->ok)) {
    $sms_id = $req->data->ok->$number;
    /// TODO: Save the SMS ID for later use.
    return array('status' => TRUE);
  }
  elseif (isset($req->data->error)) {
    // On failure, return the actual failure message:
    return array(
      'status'    => FALSE,
      'message'   => 'Service reported error: %error',
      'variables' => array('%error' => $req->data->error),
    );
  }
  else {
    // This should never occur!
    return array(
      'status'    => FALSE,
      'message'   => 'Unknown response from service: %error',
      'variables' => array('%error' => $req->data),
    );
  }
}

/**
 * Requests delivery report information for a specific SMS id.
 *
 * @param string $id SMS ID as returned by the Yuboto API when the SMS was sent.
 *
 * @return array      Information regarding the SMS, or eror.
 */
function _sms_yuboto_command_dlr($id) {
  $settings = _sms_yuboto_get_configuration();

  $params = array(
    'api_key' => $settings['yuboto_api_key'],
    'action'  => 'dlr',
    'id'      => $id,
    'details' => 'Yes',
    'options' => 'Yes',
  );

  /// TODO: Do something with the message.
  return array();
}

/**
 * Perform a raw HTTP request to the Yuboto API.
 *
 * @param array $params    An array of GET parameters to include in the request.
 * @param bool $devel      Whether to log the request and response to watchdog.
 *
 * @return object          The HTTP response, with JSON decoded data from the API.
 */
function _sms_yuboto_request(array $params, $devel = FALSE) {
  $request = SMS_YUBOTO_BASE_URI . '?' . http_build_query($params);

  $req = drupal_http_request($request, array(
    'timeout' => 5,
  ));

  if ($devel) {
    watchdog('sms_yuboto', 'Request URI: !uri', array('!uri' => $request), WATCHDOG_DEBUG);
    watchdog('sms_yuboto', 'Response: !response', array('!response' => '<pre>' . print_r($req, 1) . '</pre>'), WATCHDOG_DEBUG);
  }

  $req->data = json_decode($req->data);
  return $req;
}

function sms_yuboto_callback() {
/*
$_GET: Array
(
    [sender] => MySender
    [receiver] => 301234567890
    [smsid] => 0C4D3AFB-B370-4306-9CBB-C541DAAAC549
    [status] => 1
    [option1] => 35e2a7d35ccdfaaaefc4be481abb9e64
)
 */
}
