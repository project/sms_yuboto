<?php

/**
 * @file
 * SMS Framework forms - admin form, its validation, and send form.
 */

/**
 * Callback for the SMS Framework administration form.
 *
 * @param array $configuration Previously saved configuration values.
 *
 * @return array                Form array
 */
function _sms_yuboto_admin_form(array $configuration) {
  $items = array();
  if (count($configuration) == 0) {
    $configuration = array(
      'yuboto_api_key'        => '',
      'yuboto_allowed_types'  => array(
        'sms'     => 'sms',
        'flash'   => 1,
        'unicode' => 1,
      ),
      'yuboto_long_sms'       => 0,
      'yuboto_default_sender' => substr(variable_get('site_name', "Default sender"), 0, 16),
      'yuboto_validity'       => 4320,
      'yuboto_postdate'       => 1,
      'yuboto_devel'          => 0,
    );
  }

  $items['yuboto_api_key'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Yuboto API key'),
    '#default_value' => $configuration['yuboto_api_key'],
    '#size'          => 40,
    '#maxlength'     => 40,
    '#required'      => TRUE,
  );

  $items['yuboto_allowed_types'] = array(
    '#title'         => t('Type of SMS messages allowed'),
    '#type'          => 'checkboxes',
    '#options'       => _sms_yuboto_sms_types(),
    '#default_value' => $configuration['yuboto_allowed_types'],
    '#description'   =>
      t('Define the types of SMS to allow sending:') .
      '<ul>
        <li>' . t('Simple SMS: all the 7bit alphabet, as per GSM 03.38, including the characters ^{}\\[~]|€ which count as 2.') . '</li>
        <li>' . t('Flash SMS: show the SMS as a service SMS. It will not be saved on the device.') . '</li>
        <li>' . t('Unicode SMS: allow unicode characters in the SMS. This limits each SMS to 70 characters instead of 160.') . '</li>
      </ul>',
  );

  $items['yuboto_long_sms'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Allow sending long messages'),
    '#description'   => t('Allow sending messages over 160 (Simple SMS) or 70 (Unicode SMS) characters. You will be charged for the number of messages sent. <strong>If this is not checked, messages will be truncated to a single SMS (160/70 characters)!</strong>'),
    '#default_value' => $configuration['yuboto_long_sms'],
  );

  $items['yuboto_default_sender'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Default sender'),
    '#default_value' => $configuration['yuboto_default_sender'],
    '#size'          => 16,
    '#maxlength'     => 16,
    '#required'      => TRUE,
    '#description'   => t('Sender shown to recipients. Can be numeric (max 16 digits) or alphanumeric text (max 11 characters).You can edit this value before sending.'),
  );

  $items['yuboto_advanced'] = array(
    '#type'      => 'fieldset',
    '#title'     => 'Advanced Options',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $items['yuboto_advanced']['yuboto_postdate'] = array(
    '#type'   => 'checkbox',
    '#title'  => 'Allow post-dated SMS',
    '#default_value' => $configuration['yuboto_postdate'],
    '#description' => t('Allows to set a date for when the SMS should be delivered by Yuboto, when sending messages.'),
  );

  $items['yuboto_advanced']['yuboto_validity'] = array(
    '#type'          => 'textfield',
    '#title'         => t('SMS Validity'),
    '#default_value' => $configuration['yuboto_validity'],
    '#size'          => 4,
    '#maxlength'     => 4,
    '#required'      => TRUE,
    '#description'   => t('If the SMS is not delivered directly, the amount of seconds for which the message will remain active, before rejected by the carrier. Min value 30, max 4320'),
  );

  $items['yuboto_advanced']['yuboto_devel'] = array(
    '#type'          => 'checkbox',
    '#title'         => 'API Devel Mode',
    '#default_value' => $configuration['yuboto_devel'],
    '#description'   => t('Log messages and API responses to watchdog, do not actually send them.'),
  );

  return $items;
}

/**
 * Implements hook_formname_form_validate().
 */
function _sms_yuboto_admin_form_validate($form, &$form_state) {
  // Validate the default sender.
  if (is_numeric($form_state['values']['yuboto_default_sender']) && strlen($form_state['values']['yuboto_default_sender']) > SMS_YUBOTO_SENDER_NUM_MAX) {
    form_set_error('yuboto_default_sender', t('Numeric senders must be 16 digits max'));
  }

  if (!is_numeric($form_state['values']['yuboto_default_sender']) && strlen($form_state['values']['yuboto_default_sender']) > SMS_YUBOTO_SENDER_CHAR_MAX) {
    form_set_error('yuboto_default_sender', t('Text sender must be 11 characters max'));
  }

  if ($form_state['values']['yuboto_validity'] < SMS_YUBOTO_VALID_MIN || $form_state['values']['yuboto_validity'] > SMS_YUBOTO_VALID_MAX || !is_numeric($form_state['values']['yuboto_validity'])) {
    form_set_error('yuboto_validity', t('Validity must be a number between !min and !max', array('!min' => SMS_YUBOTO_VALID_MIN, '!max' => SMS_YUBOTO_VALID_MAX)));
  }
}

/**
 * Callback for the SMS Framework send form.
 *
 * @param bool $required Unknown??
 *
 * @return array         Form array
 */
function _sms_yuboto_send_form($required = FALSE) {
  $items = array();

  $settings = _sms_yuboto_get_configuration();
  $available_types = array_filter($settings['yuboto_allowed_types']);

  $items['yuboto_send_type'] = array(
    '#type' => 'radios',
    '#title' => 'Send as type',
    '#options' => _sms_yuboto_sms_types($available_types),
    '#required' => TRUE,
  );

  $items['yuboto_send_name'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => 'Sender name or number',
    '#default_value' => $settings['yuboto_default_sender'],
  );

  $items['yuboto_send_later'] = array(
    '#title' => 'Date to send messages',
    '#description' => t('Enter a date to send messages. <strong>Leave blank for immediate sending (that is: now).</strong> Install the date module for a friendlier input.'),
  );

  if (count(element_info('date_popup'))) {
    $items['yuboto_send_later']['#type'] = 'date_popup';
    $items['yuboto_send_later']['#tiempicker'] = 'timepicker';
  }
  elseif (element_info('date_select')) {
    $items['yuboto_send_later']['#type'] = 'date_select';
    $items['yuboto_send_later']['#default_value'] = '';
  }
  else {
    $items['yuboto_send_later']['#type'] = 'textfield';
  }

  return $items;
}
