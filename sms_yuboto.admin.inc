<?php

/**
 * @file
 * The admin sms_yuboto functions.
 */

/**
 * Overall status screen. Should include credits remaining, and any errors that have occurred.
 */
function sms_yuboto_status() {
  if(!_sms_yuboto_configured()) { return t('You have not configured this module yet! Click !here to do so.', array('!here'=> l('here', 'admin/smsframework/gateways/yuboto'))); }
  return t('This feature has not been implemented yet.');
}

/**
 * Delivery report page for recently sent SMS.
 */
function sms_yuboto_delivery() {
  if(!_sms_yuboto_configured()) { return t('You have not configured this module yet! Click !here to do so.', array('!here'=> l('here', 'admin/smsframework/gateways/yuboto'))); }
  return t('This feature has not been implemented yet.');
}

/**
 * List of post-dated SMS, and associated actions.
 */
function sms_yuboto_post_date() {
  if(!_sms_yuboto_configured()) { return t('You have not configured this module yet! Click !here to do so.', array('!here'=> l('here', 'admin/smsframework/gateways/yuboto'))); }
  return t('This feature has not been implemented yet.');
}

