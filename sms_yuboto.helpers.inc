<?php

/**
 * @file
 * Various helper functions.
 */

/**
 * The currently available SMS types.
 *
 * @param array $filter Filter non-available SMS types (as per saved checkboxes).
 */
function _sms_yuboto_sms_types(array $filter = NULL) {
  $types = array(
    'sms'     => 'Simple SMS',
    'flash'   => 'Flash SMS',
    'unicode' => 'Unicode SMS',
  );
  if (is_null($filter)) {
    return $types;
  }
  else {
    return array_intersect_key($types, $filter);
  }
}

/**
 * Shortcut to getting the Yuboto gateway configuration from the SMS Framework.
 */
function _sms_yuboto_get_configuration() {
  $gateway  = sms_gateways('gateway', 'yuboto');
  return $gateway['configuration'];
}

/**
 * Shortcut to converting a date to the API required formats.
 *
 * @param string $date A date in Y-m-d H:i format.
 */
function _sms_yuboto_transform_date($date) {
  $date_obj = date_create_from_format('Y-m-d H:i', $date);
  // If date is NULL or invalid, catch it before it breaks things.
  if (!is_object($date_obj)) {
    return array(NULL, NULL);
  }
  return array(
    $date_obj->format('Ymd'),
    $date_obj->format('Hi'),
  );
}

/**
 * Checks if we have a valid configuration.
 *
 * @return bool Whether we have a valid configuration
 */
function _sms_yuboto_configured() {
  $settings = _sms_yuboto_get_configuration();
  return isset($settings['yuboto_api_key']);
}
